﻿// C# example:
#if UNITY_EDITOR && UNITY_IOS
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections.Generic;
using UnityEditor.iOS.Xcode;
using System.IO;
using Appxplore.APCP;

public static class APCPPostProcess 
{
    [PostProcessBuild(999)]
    public static void OnPostprocessBuild(BuildTarget target, string buildTarget) 
    {
        //Debug.Log("[APCP]OnPostprocessBuild adding URL SCHEME");

        if (target == BuildTarget.iOS && APCPSetting.UrlScheme.Length > 0)  
        {
            // Get plist
            string plistPath = buildTarget + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));

            // Get root
            PlistElementDict rootDict = plist.root;
            PlistElementArray urlTypeArray = null;
            PlistElementDict urlTypeDict = null;

            if (rootDict.values.ContainsKey("CFBundleURLTypes"))
                urlTypeArray = rootDict.values["CFBundleURLTypes"].AsArray();
            else
                urlTypeArray = rootDict.CreateArray("CFBundleURLTypes");

            urlTypeDict = urlTypeArray.AddDict();
            urlTypeDict.SetString("CFBundleTypeRole", "None");
            urlTypeDict.SetString("CFBundleURLName", "APCP");
            PlistElementArray schemeArray = urlTypeDict.CreateArray("CFBundleURLSchemes");
            schemeArray.AddString(APCPSetting.UrlScheme);   //if success test app need to be changed.
            //Debug.Log("[APCP]Setting URL TYPE for infoPlist");
            // Write to file
            File.WriteAllText(plistPath, plist.WriteToString());
        }
    }
}

#endif
