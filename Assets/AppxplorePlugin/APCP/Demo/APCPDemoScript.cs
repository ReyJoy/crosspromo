﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Appxplore.APCP;

public class APCPDemoScript : MonoBehaviour 
{
    public Text msgBox;
    public ScrollRect msgBoxScroll;
    public InputField m_userIdField;
    public Dropdown m_moreGamesDropDown;
    public Dropdown m_interstitialDropDown;
    public Dropdown m_interstitialList;

    public Text m_autoInterstitialText;
    public Text m_autoMoreGameText;

    public GameObject m_generalPanel;
    public GameObject m_moreGamePanel;
    public GameObject m_interstitialPanel;

	// Use this for initialization
	void Start () 
    {
        APCPAgent.createInstanceIfNeeded();
        m_moreGamesDropDown.value = (int)APCPSetting.MoreGameAnimationType;
        m_interstitialDropDown.value = (int)APCPSetting.InterstitialAnimationType;

        UpdateMoreGameCache();
        UpdateInterstitialCache();
	}
	
    public void OnInitClicked()
    {
        AppendMsgBox("OnInitClicked");
        APCPAgent.Instance.initClient();
    }

    public void OnConnectClicked()
    {
        AppendMsgBox("OnConnectClicked");
        APCPAgent.Instance.connect(OnInitCallback);
    }

    public void OnIsMoreGameCachedClicked()
    {
        
        AppendMsgBox("OnIsMoreGameCachedClicked: " + APCPAgent.Instance.isMoreGameReady());
    }

    public void OnCacheMoreGameClicked()
    {
        AppendMsgBox("OnCacheMoreGameClicked");
        APCPAgent.Instance.cacheMoreGame();
    }

    public void OnShowMoreGameClicked()
    {
        APCPAgent.Instance.setMoreGameAnimationType((APCPDefine.AnimationType)m_moreGamesDropDown.value);
        AppendMsgBox("OnShowMoreGameClicked: " + APCPAgent.Instance.showMoreGame(OnMoreGameCallback));
    }

    public void OnIsInterstitialCachedClicked()
    {
        
        AppendMsgBox("OnIsInterstitialCachedClicked=>" + m_interstitialList.captionText.text + ": " + APCPAgent.Instance.isInterstitialReady(m_interstitialList.captionText.text));
    }

    public void OnCacheInterstitialClicked()
    {
        AppendMsgBox("OnCacheInterstitialClicked=>" + m_interstitialList.captionText.text);
        APCPAgent.Instance.cacheInterstitial(m_interstitialList.captionText.text);
    }

    public void OnCacheAllInterstitialsClicked()
    {
        APCPAgent.Instance.cacheAllInterstitials();
    }

    public void OnShowInterstitialClicked()
    {
        APCPAgent.Instance.setInterstitialAnimationType((APCPDefine.AnimationType)m_interstitialDropDown.value);
        AppendMsgBox("OnShowInterstitialClicked=> " + m_interstitialList.captionText.text +" : " + APCPAgent.Instance.showInterstitial(m_interstitialList.captionText.text, OnInterstitialCallback));
    }

    public void OnSetUserIdFieldClicked()
    {
        AppendMsgBox("SetUserId: " + m_userIdField.text);
        APCPAgent.Instance.setCustomUserId(m_userIdField.text);
    }

    public void OnGetDeepLinkDataClicked()
    {
        AppendMsgBox("OnGetDeepLinkDataClicked=> " + APCPAgent.Instance.getDeepLinkData());
    }

    public void OnDeleteDeepLinkDataClicked()
    {
        AppendMsgBox("OnDeleteDeepLinkDataClicked");
        APCPAgent.Instance.removeDeepLinkData();
    }

    public void OnSendDeepLinkDataClicked()
    {
        AppendMsgBox("OnSendDeepLinkDataClicked: testapp://host/page1" );
        Application.OpenURL("testapp://host/page1");
    }

    public void OnGeneralToggleOnOff(bool _isOnOff)
    {
        Debug.Log("OnGeneralOnOff: " + _isOnOff);
        m_generalPanel.SetActive(_isOnOff);
    }

    public void OnMoreGameToggleOnOff(bool _isOnOff)
    {
        m_moreGamePanel.SetActive(_isOnOff);
    }

    public void OnInterstitialToggleOnOff(bool _isOnOff)
    {
        m_interstitialPanel.SetActive(_isOnOff);
    }
        
    public void OnQuitClicked()
    {
        Application.Quit();
    }

    public void OnClearMessageBoxClicked()
    {
        msgBox.text = " ";
        msgBox.text = string.Empty;
    }

    public void OnAutoInterstitialClicked()
    {
        APCPAgent.Instance.setAutoCacheInterstitial(!APCPSetting.isAutoCachedInterstitial);
        UpdateInterstitialCache();
    }

    public void OnAutoMoreGameClicked()
    {
        APCPAgent.Instance.setAutoCacheMoreGame(!APCPSetting.isAutoCachedMoreGame);
        UpdateMoreGameCache();
    }

    public void OnCheckConsentClicked()
    {
        AppendMsgBox("OnCheckConsentClicked=> " + APCPAgent.Instance.isConsentGiven());
    }

    public void OnSetConsentFalseClicked()
    {
        AppendMsgBox("OnSetConsentFalseClicked!");
        APCPAgent.Instance.setConsent(false);
    }

    public void OnShowConsentClicked()
    {
        AppendMsgBox("OnShowConsentClicked!");
        APCPAgent.Instance.showConsent("http://appxplore.com/index.php/contact-us/privacy-policy", OnConsentCallback);
    }

    void AppendMsgBox(string msg)
    {
        msgBox.text = msgBox.text + msg + "\r\n";    
        msgBoxScroll.verticalNormalizedPosition = 0f;
    }

    void OnInitCallback(bool _success)
    {
        AppendMsgBox("Init Result: " + _success);
    }

    void OnMoreGameCallback()
    {
        AppendMsgBox("More Game Closed");
    }

    void OnInterstitialCallback()
    {
        AppendMsgBox("Interstitial Closed");
    }

    void OnConsentCallback(bool _consentGiven)
    {
        AppendMsgBox("OnConsentCallback: " + _consentGiven);
    }

    void UpdateMoreGameCache()
    {
        m_autoMoreGameText.text = "Auto Cache More Game Now: \n" + APCPSetting.isAutoCachedMoreGame;
    }

    void UpdateInterstitialCache()
    {
        m_autoInterstitialText.text = "Auto Cache Interstitial Now: \n" + APCPSetting.isAutoCachedInterstitial;
    }
}
