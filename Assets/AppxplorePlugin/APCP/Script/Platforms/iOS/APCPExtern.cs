﻿// Copyright (C) 2015 Google, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#if UNITY_IOS
using System.Collections;

using System;
using System.Runtime.InteropServices;

namespace Appxplore.APCP.iOS
{
    // Externs used by the iOS component.
    internal class APCPExterns
    {
        #region Initialize externs

        [DllImport("__Internal")]
        internal static extern void APCPConstructWithCallbacks(
            APCPClient.APCPClientInitDoneCallback initDoneCallback, 
            APCPClient.APCPClientInitFailCallback initFailCallback,
            APCPClient.APCPClientMoreGameCloseCallback moreGameCloseCallback,
            APCPClient.APCPClientInterstitialCloseCallback interstitialCloseCallback,
            APCPClient.APCPClientConsentReturnCallback consentReturnCallback);

        [DllImport("__Internal")]
        internal static extern void APCPInitialize(string gameId, bool isDebugMode);

        [DllImport("__Internal")]
        internal static extern void APCPConnect();

        [DllImport("__Internal")]
        internal static extern void APCPSetCustomId(string userId);

        [DllImport("__Internal")]
        internal static extern void APCPSetCustomLang(string customLanguage);

        [DllImport("__Internal")]
        internal static extern void APCPSetMoreGameCacheOnStartOnly(bool cacheStart);

        [DllImport("__Internal")]
        internal static extern void APCPSetMoreGameAutoCache(bool autoCache);

        [DllImport("__Internal")]
        internal static extern void APCPSetMoreGameAnimationType(int type);

        [DllImport("__Internal")]
        internal static extern bool APCPCheckMoreGameReady();

        [DllImport("__Internal")]
        internal static extern void APCPCacheMoreGame();

        [DllImport("__Internal")]
        internal static extern bool APCPShowMoreGame();


        [DllImport("__Internal")]
        internal static extern void APCPSetInterstitialAutoCache(bool autoCache);

        [DllImport("__Internal")]
        internal static extern void APCPSetInterstitialsLocationList(string[] locations, int length);

        [DllImport("__Internal")]
        internal static extern void APCPSetInterstitialAnimationType(int type);

        [DllImport("__Internal")]
        internal static extern bool APCPCheckInterstitialReady(string locationKey);

        [DllImport("__Internal")]
        internal static extern void APCPCacheInterstitial(string locationKey);

        [DllImport("__Internal")]
        internal static extern void APCPCacheAllInterstitials();

        [DllImport("__Internal")]
        internal static extern bool APCPShowInterstitial(string locationKey);


        [DllImport("__Internal")]
        internal static extern string APCPGetDeepLinkData();

        [DllImport("__Internal")]
        internal static extern void APCPRemoveDeepLinkData();


        [DllImport("__Internal")]
        internal static extern void APCPSetConsent(bool consentGiven);

        [DllImport("__Internal")]
        internal static extern bool APCPIsConsentGiven();

        [DllImport("__Internal")]
        internal static extern void APCPShowConsentDialog(string privacyPolicyUrl);



        #endregion
    }
}

#endif
