﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Appxplore.APCP.Common;
#if UNITY_IOS
using Appxplore.APCP.iOS;
#else
using Appxplore.APCP.Android;
#endif

namespace Appxplore.APCP.Platforms
{
    internal class APCPClientFactory
    {

        public static IAPCPClient buildAPCPClient()
        {
            #if UNITY_EDITOR || UNITY_STANDALONE
            IAPCPClient client = new DummyClient();
            #else
            IAPCPClient client = new APCPClient();
            #endif
            return client;
        }
    }
}
