﻿using System;
using System.Collections;

namespace Appxplore.APCP.Common
{
    internal interface IAPCPClient
    {
        event Action OnInitDone;
        event Action<string> OnInitFailed;
        event Action OnMoreGameClosed;
        event Action OnInterstitialClosed;
        event Action<bool> OnConsentReturned;

        void initialize(string gameId, bool isDebug);
        void connect();
        void setCustomUserId(string userId);
        void setCustomLanguage(string userId);

        void setMoreGameAutoCache(bool _auto);
        void setMoreGameCacheOnStartOnly(bool _startOnly);
        void setMoreGameAnimationType(APCPDefine.AnimationType _type);
        bool isMoreGameCached();
        void cacheMoreGames();
        bool showMoreGames();

        void setInterstitialAutoCache(bool _auto);
        void setInterstitialLocationList(string[] arrayOfLocations);
        void setInterstitialAnimationType(APCPDefine.AnimationType _type);
        bool isInterstitialCached(string location);
        void cacheInterstitial(string location);
        void cacheAllInterstitials();
        bool showInterstitial(string location);

        void removeDeepLinkData();
        string getDeepLinkData();

        void setConsent(bool _consentGiven);
        bool isConsentGiven();
        void showConsentDialog(string _privacyPolicyUrl);
    }
}
