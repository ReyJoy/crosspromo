﻿using UnityEngine;
using System.Collections;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System;

public class AppxploreUtils
{
	private static string m_internalPersistentDataPath = null;

	/// <summary>
	/// Determines if is directory writable the specified path.
	/// </summary>
	/// <returns><c>true</c> if is directory writable the specified path; otherwise, <c>false</c>.</returns>
	/// <param name="path">Path.</param>
	public static bool IsDirectoryWritable(string path) 
	{
		try 
		{
			if (!Directory.Exists(path)) 
				return false;
			
			string file = Path.Combine(path, Path.GetRandomFileName());
			using (FileStream fs = File.Create(file, 1)) {}
			File.Delete(file);
			return true;
		}
		catch 
		{
			return false;
		}
	}

	/// <summary>
	/// Internal Functions that combine all the path component to get a persistent data path. 
	/// Will test the path directory is writable or not.
	/// return null if that path is not exists.
	/// </summary>
	/// <returns>The persistent data path/null</returns>
	/// <param name="components">Components of the paths</param>
	private static string GetPersistentDataPath(params string[] components) 
	{
		try 
		{
			string path = Path.DirectorySeparatorChar + string.Join("" + Path.DirectorySeparatorChar, components); 
			if (!Directory.GetParent(path).Exists) 
				return null;
			
			if (!Directory.Exists(path)) 
			{
				Debug.Log("creating directory: " + path);
				Directory.CreateDirectory(path);
			}

			if (!IsDirectoryWritable(path)) 
			{
				Debug.LogWarning("persistent data path not writable: " + path);
				return null;
			}

			return path;
		} 
		catch (Exception ex) 
		{
			Debug.LogException(ex);
			return null;
		}
	}

	/// <summary>
	/// Use the Application.persistentDataPath first, if not applicable, will use some path crafted ourselves.
	/// </summary>
	/// <value>The Persistent Data Path for writing in game.</value>
	public static string persistentDataPathInternal 
	{
		

		get 
		{ 
			if(string.IsNullOrEmpty(m_internalPersistentDataPath))
			{
				m_internalPersistentDataPath = Application.persistentDataPath; 
#if UNITY_ANDROID
				if (!IsDirectoryWritable(m_internalPersistentDataPath)) 
                #if UNITY_5_6_OR_NEWER
                    m_internalPersistentDataPath = GetPersistentDataPath("storage", "emulated", "0", "Android", "data", Application.identifier, "files"); 
                #else
                    m_internalPersistentDataPath = GetPersistentDataPath("storage", "emulated", "0", "Android", "data", Application.bundleIdentifier, "files"); 
                #endif

				if (string.IsNullOrEmpty(m_internalPersistentDataPath)) 
                #if UNITY_5_6_OR_NEWER
                    m_internalPersistentDataPath = GetPersistentDataPath("data", "data", Application.identifier, "files");
                #else
                    m_internalPersistentDataPath = GetPersistentDataPath("data", "data", Application.bundleIdentifier, "files");
                #endif
#endif
			}

			return m_internalPersistentDataPath;
		}


	}


	/// <summary>
	/// Append the File Name to Persistent Data Path
	/// </summary>
	/// <returns>Path to the File which includes persistent data path</returns>
	/// <param name="_fileName">File name.</param>
	public static string appendPersistentPath(string _fileName, string _fileType)
	{
		return string.Format("{0}/{1}.{2}", persistentDataPathInternal, _fileName, _fileType);
	}

	/// <summary>
	/// Save to File with type dat, auto append to persistent data path
	/// </summary>
	/// <param name="filename">Filename starts from Persistent Data Path</param>
	/// <param name="data">Data in byte array</param>
	public static void SaveToFile(string filename, byte[] data)
	{
		if (string.IsNullOrEmpty (filename))
			throw new System.ArgumentNullException ("filename");

		File.WriteAllBytes (appendPersistentPath(filename, "dat"), data);
	}

	/// <summary>
	/// Read the Byte array from  given file name, auto append persistent data path
	/// </summary>
	/// <returns><c>true</c>, if from file was  read, <c>false</c> otherwise.</returns>
	/// <param name="filename">Filename starts from Persistent Data Path.</param>
	/// <param name="data">Data read in the file</param>
	public static bool ReadFromFile(string filename, out byte[] data)
	{
		data = null;

		if (string.IsNullOrEmpty (filename))
			return false;

		string fileName = appendPersistentPath(filename, "dat");

		if (File.Exists (fileName))
		{
			data = System.IO.File.ReadAllBytes(fileName);
		}

		return true;
	}

	/// <summary>
	/// Save to File with type txt, auto append to persistent data path
	/// </summary>
	/// <param name="filename">Filename starts from Persistent Data Path</param>
	/// <param name="data">Data in string</param>
	public static void SaveToFile(string filename, string data)
	{
		if (string.IsNullOrEmpty (filename))
			throw new System.ArgumentNullException ("filename");

		File.WriteAllText (appendPersistentPath(filename, "txt"), data);
	}

	/// <summary>
	/// Read the string from  given file name, auto append persistent data path
	/// </summary>
	/// <returns><c>true</c>, if from file was  read, <c>false</c> otherwise.</returns>
	/// <param name="filename">Filename starts from Persistent Data Path.</param>
	/// <param name="data">Data read in the file</param>
	public static bool ReadFromFile(string filename, out string data)
	{
		data = string.Empty;
		if (string.IsNullOrEmpty (filename))
			return false;


		string fileName = appendPersistentPath(filename, "txt");
		if (File.Exists (fileName))
		{
			data = System.IO.File.ReadAllText(fileName);
		}

		return true;
	}
		
	/// <summary>
	/// Hash an input string and return the hash as 
	/// a 32 character hexadecimal string. 
	/// </summary>
	/// <returns>The md5 hash.</returns>
	/// <param name="input">Input String</param>
	public static string getMd5Hash(string input)
	{
		// Create a new instance of the MD5CryptoServiceProvider object.
		MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

		// Convert the input string to a byte array and compute the hash. 
		byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

		// Create a new Stringbuilder to collect the bytes 
		// and create a string.
		StringBuilder sBuilder = new StringBuilder();

		// Loop through each byte of the hashed data  
		// and format each one as a hexadecimal string. 
		for (int i = 0; i < data.Length; i++)
		{
			sBuilder.Append(data[i].ToString("x2"));
		}

		// Return the hexadecimal string. 
		return sBuilder.ToString();
	}

	/// <summary>
	/// A way to make input string become byte array and base64 the input
	/// </summary>
	/// <param name="_str">String.</param>
	public static string make64(string _str)
	{
		byte[] temp = Encoding.UTF8.GetBytes (_str);
		return System.Convert.ToBase64String(temp);
	}

	/// <summary>
	/// A way to revert back a Base64 string to normal string
	/// </summary>
	/// <param name="temp">Base 64 String.</param>
	public static string get64(string temp)
	{
		byte[] _bytes = System.Convert.FromBase64String (temp);
		return Encoding.UTF8.GetString (_bytes);
	}

	/// <summary>
	/// Convert String into byte array
	/// </summary>
	/// <returns>byte array</returns>
	/// <param name="str">input string.</param>
	public static byte[] GetBytes(string str)
	{
		byte[] bytes = new byte[str.Length * sizeof(char)];
		System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
		return bytes;
	}

	/// <summary>
	/// Convert byte array into string
	/// </summary>
	/// <returns>string</returns>
	/// <param name="bytes">byte array</param>
	public static string GetString(byte[] bytes)
	{
		try
		{
			char[] chars = new char[bytes.Length / sizeof(char)];
			System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
			return new string(chars);
		}
		catch
		{
			Debug.Log ("error in GetString" );
			return null;
		}
	}

	public static void QuitApplication() 
	{
		#if UNITY_ANDROID
		using (AndroidJavaClass javaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) 
		{
			AndroidJavaObject unityActivity = javaClass.GetStatic<AndroidJavaObject>("currentActivity");
			unityActivity.Call<bool>("moveTaskToBack", true);
			unityActivity.Call ("finish");
		}
		#else
		Application.Quit();
		#endif
	}
}
