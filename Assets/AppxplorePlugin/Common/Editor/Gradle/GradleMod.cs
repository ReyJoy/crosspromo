﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace Appxplore.EditorUtils
{
    public class GradleMod 
    {
       
        [MenuItem("AppxplorePlugin/Gradle & Proguard/Enable", false, 1)]
    	public static void EnableProguard()
    	{
    		string _msg = string.Empty;
    		if (EditorUserBuildSettings.androidBuildSystem != AndroidBuildSystem.Gradle) {
    			EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Gradle;
    			_msg += "- Enable Gradle as Build Option\n";
    		}

#if UNITY_2017_4_OR_NEWER
            EditorUserBuildSettings.androidReleaseMinification = AndroidMinification.Proguard;
            EditorUserBuildSettings.androidDebugMinification = AndroidMinification.Proguard;

            _msg += "-Enabled Minify Option.\n";
            _msg += "-In order to turn on Gradle Template, please check the \"Custom Gradle Template\" from PlayerSettings -> PublishingSettings.\n";
#else
            if (!File.Exists(Application.dataPath + "/Plugins/Android/mainTemplate.gradle"))
    		{
    			File.Copy(Application.dataPath + "/AppxplorePlugin/Common/Editor/Gradle/mainTemplate.gradle", Application.dataPath + "/Plugins/Android/mainTemplate.gradle");
    			_msg += "- Added mainTemplate gradle.\n";
    		}
#endif

            string _proguardContent = string.Empty;
    		string[] files = Directory.GetFiles( Application.dataPath, "*.pgrules", SearchOption.AllDirectories );
    		foreach( string file in files ) {
    			_proguardContent += File.ReadAllText (file)+"\n";
    		}

    		File.WriteAllText (Application.dataPath + "/Plugins/Android/proguard-user.txt", _proguardContent);
    		_msg += "- set proguard-user.txt Content\n";

            _msg += ManifestMod.EnableGradleModAndroidManifest();

            AssetDatabase.Refresh();
    		EditorUtility.DisplayDialog ("Enable Proguard", _msg, "OK");
    	}

        [MenuItem("AppxplorePlugin/Gradle & Proguard/Enable", true, 1)]
        public static bool ValidateProguard()
        {
            return (EditorUserBuildSettings.activeBuildTarget == BuildTarget.Android && EditorUserBuildSettings.androidBuildSystem != AndroidBuildSystem.Gradle);
        }

        [MenuItem("AppxplorePlugin/Gradle & Proguard/Disable", false, 1)]
        public static void DisableGradleProguard()
        {
            string _msg = string.Empty;
            if (EditorUserBuildSettings.androidBuildSystem == AndroidBuildSystem.Gradle) {
                EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Gradle;
                _msg += "-Disable Gradle as Build Option\n";
            }

            if (File.Exists(Application.dataPath + "/Plugins/Android/mainTemplate.gradle"))
            {
                AssetDatabase.DeleteAsset("Assets/Plugins/Android/mainTemplate.gradle");
                _msg += "-Deleted mainTemplate gradle.\n";
            }

#if UNITY_2017_4_OR_NEWER
            EditorUserBuildSettings.androidReleaseMinification = AndroidMinification.None;
            EditorUserBuildSettings.androidDebugMinification = AndroidMinification.None;
            _msg += "-Disabled Minify Option.\n";
#endif

            if (File.Exists(Application.dataPath + "/Plugins/Android/proguard-user.txt"))
            {
                AssetDatabase.DeleteAsset("Assets/Plugins/Android/proguard-user.txt");
                _msg += "-Deleted proguard-user.txt.\n";
            }

            _msg += ManifestMod.DisableGradleModAndroidManifest();
                
            AssetDatabase.Refresh();
            EditorUtility.DisplayDialog ("Disable Proguard", _msg, "OK");
        }

        [MenuItem("AppxplorePlugin/Gradle & Proguard/Disable", true, 1)]
        public static bool ValidateDisableProguard()
        {
            return (EditorUserBuildSettings.activeBuildTarget == BuildTarget.Android && EditorUserBuildSettings.androidBuildSystem == AndroidBuildSystem.Gradle);
        }


    }
}
